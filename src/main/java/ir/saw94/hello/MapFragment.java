package ir.saw94.hello;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.mapbox.mapboxsdk.api.ILatLng;
import com.mapbox.mapboxsdk.tileprovider.tilesource.ITileLayer;
import com.mapbox.mapboxsdk.tileprovider.tilesource.WebSourceTileLayer;
import com.mapbox.mapboxsdk.views.MapView;


/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends Fragment {


    private MapView mapView;

    public MapFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_map, container, false);

        mapView = (MapView) view.findViewById(R.id.mapView);
        ITileLayer source = new WebSourceTileLayer("openstreetmap",
                "http://tile.openstreetmap.org/{z}/{x}/{y}.png").setName("OpenStreetMap")
                .setAttribution("© OpenStreetMap Contributors")
                .setMinimumZoomLevel(1)
                .setMaximumZoomLevel(18);

        mapView.setTileSource(source);
        mapView.setZoom(12);
        mapView.setCenter(new ILatLng() {
            @Override
            public double getLatitude() {
                return 36.36;
            }

            @Override
            public double getLongitude() {
                return 59.59;
            }

            @Override
            public double getAltitude() {
                return 0;
            }
        });

        Button btnZoomIn = (Button) view.findViewById(R.id.btnZoomIn);
        btnZoomIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                zoomIn();
            }
        });

        Button btnZoomOut = (Button) view.findViewById(R.id.btnZoomOut);
        btnZoomOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                zoomOut();
            }
        });



        return view;
    }

    public void zoomIn()
    {
        mapView.zoomIn();
        Log.d("test", "zoom: " + mapView.getZoomLevel());
    }

    public void zoomOut()
    {
        mapView.zoomOut();
        Log.w("test","zoom: "+mapView.getZoomLevel());
    }


}
