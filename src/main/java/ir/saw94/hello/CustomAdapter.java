package ir.saw94.hello;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by hp on 8/18/15.
 */
public class CustomAdapter extends ArrayAdapter<String> {
    public CustomAdapter(Context context, int resource, String[] strings) {
        super(context, resource, strings);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View view = layoutInflater.inflate(R.layout.costom_row, null);

        String page = getItem(position);

        TextView tvPage = (TextView) view.findViewById(R.id.tvPage);
        tvPage.setText(page);

        return view;
    }
}
